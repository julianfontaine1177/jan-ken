import string
import random
import os
import sys

# function for the game
def RockPaperScissors():
    RPS = ["Rock", "Paper", "Scissors"]
    # for loop to make strings in array lowercase for case-insensitive
    for i in range(len(RPS)):
        RPS[i] = RPS[i].lower()

    while True:
        # Chooses random string from array
        computer = random.choice(RPS)

        # print("Lets play Rock, Paper, Scissor")
        player = input("Lets play Rock, Paper, Scissor\n")
        player.lower()
        print(computer)

        # Conditional statements for the game
        if player != "rock" and player != "paper" and player != "scissors":
            print("Invaild input. Try again")
            continue
        elif player == "rock" and computer.lower() == "paper":
            print("You Lose!")
        elif player == "rock" and computer.lower() == "scissors":
            print("You Win")
        elif player == "paper" and computer.lower() == "rock":
            print("You Win!!")
        elif player == "paper" and computer.lower() == "scissors":
            print("You Lose!!")
        elif player == "scissors" and computer.lower() == "rock":
            print("You Lose!!")
        elif player == "scissors" and computer.lower() == "paper":
            print("You Win!!")
        elif player == computer:
            print("Slatemate")
            continue

        break


RockPaperScissors()

# Loop to end/restart the program
while True:
    GG = input("Would you like to play again: y/n\n")
    GG.lower()
    if GG != "y" and GG != "n":
        continue
    elif GG == "y":
        RockPaperScissors()
    elif GG == "n":
        break